
# Efficient Concept Drift Handling for Batch Android Malware Detection Models


## Description
Project with the code for the paper entitled "Efficient Concept Drift Handling for Batch Android Malware Detection Models". 


This project contains the class files of the detectors analyzed in our work. The code used for this project depends on the code of our previous research paper and is available in this repository(https://gitlab.com/serralba/androidmaldet_comparative.git). To avoid redundant code, this project just provides the implementation of the detectors. Follow the instructions in our previous project to build the dataset and run this code.

Each class file implements the following techniques for retraining:
* Periodic retraining at fixed time chunks
* Based on the change detection mechanism that uses the PH test (0.02 threshold)

and the folowing tecniques for sample selection:
* Use all the data (baseline)
* Sliding windows of fixed size
* Uncertainty sampling
* The problem-specific sample selection (PSCS) method

Note that the selected ids for CADE(https://www.usenix.org/conference/usenixsecurity21/presentation/yang-limin) and HICL(https://arxiv.org/abs/2302.04332) have been precomputed using the original implementations to save time. We refer the interested reader to their original code implementations in GitHub: CADE(https://github.com/whyisyoung/CADE) and HICL(https://github.com/wagner-group/active-learning).

## Usage
For any method usage. You must first instantiate the method with the desired configuration.

### Retraining options

To perform periodic retraining, just disable the change detection mechanism using the change_det parameter and call evaluate with "online=True" with each chunk of the evaluation data and the method will retrain a new model with each call to evaluate:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, change_det=False )
dre.parseAPKs(ALL_apk_files_in_dataset)
dre.fit(Xtrain, Ytrain)
dre.evaluate(X1, Y1, online=True)
...
dre.evaluate(X12, Y12, online=True)
```

To perform retraining using whenever the change detection mechanism raises a drift alarm. Set the "change_det" parameter to True:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, change_det=True )
dre.parseAPKs(ALL_apk_files_in_dataset)
dre.fit(Xtrain, Ytrain)
dre.evaluate(X1, Y1, online=True)
...
dre.evaluate(X12, Y12, online=True)
```

### Sample Selection Options

For example, a sliding window of size 1000:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='SW', bs=1000 )
```

For the baseline configuration that uses all data, just select a large enough sliding window. For example 100,000:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='SW', bs=100000 )
```

For uncertainty sampling and a labeling budget of N samples for each retraining period:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='uncertainty', lab_budget=N )
```

For the PSCS the method with epsilon clustering radius of 0.01:

```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='PSCS', epsilon=0.01, lab_budget=None )
```

The PSCS method also admits to set a labeling budget using the parameter "lab_budget". For example, to label the top 150 clusters (according to the neighborhood density):
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='PSCS', epsilon=0.01, lab_budget=150 )
```

For CADE and HICL with precomputed indexes for our dataset, just:
```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='CADE', lab_budget=150 )
```

or:

```python3
from Drebin import Drebin
dre = Drebin(run_id = YOUR_RUN_ID, data_path = DATASET_PATH, working_path= OUTPUT_DIR, sample_sel=True, measure='HICL', lab_budget=150 )
```


## Authors and acknowledgment
Please cite:

```

```

## License
This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License. For more information check the link below:

http://creativecommons.org/licenses/by-nc/4.0/


## Contributing
Feel free to make any pull request


